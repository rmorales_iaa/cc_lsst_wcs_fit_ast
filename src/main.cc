//============================================================================
// Name        : cc_lsst_wcs_fit.cpp
// Author      : RMM
// Version     :
// Description : adaptation of wcs fitting algorithm of lsst
//  https://github.com/orgs/lsst/repositories
//  https://github.com/lsst/meas_astrom/tree/main/src
//  https://pipelines.lsst.io/modules/lsst.meas.astrom/tasks/lsst.meas.astrom.FitSipDistortion.html
//============================================================================

//----------------------------------------------------------------------------
#include <iostream>
#include <iomanip>
using namespace std;
//----------------------------------------------------------------------------
#include "lsst/afw/fits.h"
#include "lsst/afw/geom/SkyWcs.h"

#include "lsst/afw/table/Catalog.h"
#include "lsst/afw/table/Match.h"
#include "lsst/afw/table/Source.h"
#include "lsst/afw/geom/wcsUtils.h"

#include "lsst/geom/Angle.h"
#include "lsst/geom/SpherePoint.h"

#include "lsst/meas/astrom/sip/MatchSrcToCatalogue.h"
#include "lsst/meas/astrom/sip/CreateWcsWithSip.h"
//----------------------------------------------------------------------------
#include "csv.hpp"
//----------------------------------------------------------------------------
namespace afwTable = lsst::afw::table;
namespace astrom   = lsst::meas::astrom;
namespace fits     = lsst::afw::fits;
namespace geom     = lsst::geom;
namespace afwGeom  = lsst::afw::geom;
//----------------------------------------------------------------------------
std::shared_ptr<afwTable::SourceTable> getGlobalTable(void) {
    static std::shared_ptr<afwTable::SourceTable> table;
    if (!table) {
        afwTable::Schema schema = afwTable::SourceTable::makeMinimalSchema();
        afwTable::Point2DKey::addFields(schema, "centroid", "dummy centroid", "pixel");
        table = afwTable::SourceTable::make(schema);
        table->defineCentroid("centroid");
    }
    return table;
}
//----------------------------------------------------------------------------
auto make_record(
		  double pix_x
		, double pix_y
		, double ra
		, double dec
		, afwTable::SourceCatalog &set
		, int &id)  {
	std::shared_ptr<afwTable::SourceRecord> src = set.addNew();
	src->setId(id);
	src->set(set.getTable()->getCentroidSlot().getMeasKey(),lsst::geom::Point2D(pix_x,pix_y));
	src->set(afwTable::SourceTable::getCoordKey().getRa(), ra * geom::degrees);
	src->set(afwTable::SourceTable::getCoordKey().getDec(), dec * geom::degrees);
	++id;
}
//----------------------------------------------------------------------------
//see "lsst::meas::astrom::sip::CreateWcsWithSip"

void fit_wcs(std::string const & fits_file_name
		     , std::string const & csv_file_name
			 , geom::Angle match_radius_mas
			 , int wcs_order
			 , std::string const &csv_cat_ra_col
			 , std::string const &csv_cat_dec_col
			 , std::string const &csv_img_pix_x_col
			 , std::string const &csv_img_pix_y_col
			 , int csv_img_pix_offset
			 , bool const verbose) {


    auto fits = fits::readMetadata(fits_file_name,0,false);
	auto sky_wcs = afwGeom::makeSkyWcs(*fits);

	//build the catalogues
	afwTable::SourceCatalog cat(getGlobalTable());
	afwTable::SourceCatalog img_cat(getGlobalTable());

	csv::CSVReader reader(csv_file_name);
	int i = 0;
	int k = 0;

	std::cout << std::setprecision(20);
	if (verbose) std::cout << "+++++++++++++++++++++++++++++++++\n";

	for (csv::CSVRow& row: reader) { // Input iterator

		double cat_ra = row[csv_cat_ra_col].get<double>();
		double cat_dec = row[csv_cat_dec_col].get<double>();
		auto cat_pix = sky_wcs->skyToPixel(geom::SpherePoint(cat_ra * geom::degrees,cat_dec * geom::degrees));

		//catalogue record
		make_record(cat_pix[0]
			      , cat_pix[1]
				  , cat_ra
				  , cat_dec
				  , cat
				  , i);

		//image catalogue record
		double img_pix_x = row[csv_img_pix_x_col].get<double>() + csv_img_pix_offset;
		double img_pix_y = row[csv_img_pix_y_col].get<double>() + csv_img_pix_offset;
		auto img_ra_dec = sky_wcs->pixelToSky(geom::Point2D(img_pix_x,img_pix_y));
		make_record(img_pix_x
			      , img_pix_y
				  , img_ra_dec.getRa().asDegrees()
				  , img_ra_dec.getDec().asDegrees()
				  , img_cat
				  , i);

		if (verbose) {
			std::cout << k << " "
			          << cat_ra << " " << cat_dec << " "
			 		  << cat_pix.getX() <<  " " << cat_pix.getY() << " "
					  << img_ra_dec.getRa().asDegrees() <<  " " << img_ra_dec.getDec().asDegrees() << " "
					  << img_pix_x << " " <<  img_pix_y
					  << "\n";
		}

		k += 1;
	}

	if (verbose) std::cout << "---------------------------------\n";

	afwTable::MatchControl matchControl; //OnlyClosest(true), symmetricMatch(true), includeMismatches(false)
	auto matches = afwTable::matchRaDec(cat, img_cat, match_radius_mas, matchControl);

	if (verbose) {
		std::cout << "Sources matched : " << matches.size() << "\n";
		for (auto ptr = matches.begin(); ptr != matches.end(); ++ptr) {

		   auto src_x = ptr->first->getX();
		   auto src_y = ptr->first->getY();

		   auto dest_x = ptr->second->getX();
		   auto dest_y = ptr->second->getY();

		   std::cout << std::setprecision(20)
			         << "src (: " << src_x << ","  <<  src_y<< ")\n"
			 	     << "dest(: " << dest_x << "," <<  dest_y<< ")\n\n";
		 }
	}


	try {
	  auto new_wcs_fit = astrom::sip::makeCreateWcsWithSip(matches, *sky_wcs, wcs_order);
	  auto new_wcs = new_wcs_fit.getNewWcs();
	  auto new_metadata = afwGeom::makeTanSipMetadata(
				           new_wcs->getPixelOrigin()
		                 , new_wcs->getSkyOrigin()
		  				 , new_wcs->getCdMatrix()
		  				 , new_wcs_fit.getSipA()
		  				 , new_wcs_fit.getSipB()
		  				 , new_wcs_fit.getSipAp()
		  				 , new_wcs_fit.getSipBp()
		               );
	  std::cout << new_metadata->toString();
	}
    catch (lsst::pex::exceptions::LengthError const& e) {
      std::cout << "Too few correspondences" << "\n";
    }
    catch (lsst::pex::exceptions::TypeError const& e) {
      std::cout << "Frame type error" << "\n";
    }
    catch (std::exception const& e) {
      std::cout << "Error fitting wcs" << "\n";
    }
}

//----------------------------------------------------------------------------
void help(void) {
	std::cout << "Syntax:  cc_lsst_wcs_fit fits_file cat_csv match_radius_mas sip_order csv_cat_ra_col csv_cat_dec_col csv_img_pix_x_col csv_img_pix_y_col csv_img_pix_offset " << "\n";
	std::cout << "Example: cc_lsst_wcs_fit a.fits    a.csv   250              4         cat_ra         cat_dec         csv_img_pix_x     csv_img_pix_y     -1" << "\n";
}
//----------------------------------------------------------------------------
int main(int argc, char** argv) {

  std::cout << "\n#-------------- Starting of 'cc_lsst_wcs_fit' -------------- " << "\n";;
  if (argc != 10) help();
  else {
	bool verbose = false;
    fit_wcs(argv[1]
	  	    , argv[2]
		    , std::stod(argv[3]) * geom::milliarcseconds
		    , std::stoi(argv[4])
			, argv[5]
			, argv[6]
			, argv[7]
			, argv[8]
			, std::stoi(argv[9])
			, verbose);
  }

  std::cout << "#-------------- End of 'cc_lsst_wcs_fit' ------------------- " << "\n";

  return 0;
}
//----------------------------------------------------------------------------
